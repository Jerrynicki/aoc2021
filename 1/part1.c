#include <stdio.h>
#include <stdlib.h>

int main()
{
    int cur;
    int last = -1;
    int larger = 0;
    char buf[64];
    FILE* f = fopen("input", "rb");

    while (fgets(buf, sizeof(buf), f))
    {
        if (last > -1)
        {
            cur = atoi(buf);
            if (cur > last) {
                larger++;
            }
        }

        last = cur;
    }

    fclose(f);
    printf("%d\n", larger);
}
