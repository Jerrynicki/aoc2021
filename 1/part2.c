#include <stdio.h>
#include <stdlib.h>

int main()
{
    int larger = 0;

    int windows[8] = {0,0,0,0,0,0,0,0};
    int c = 0;
    char buf[64];
    FILE* f = fopen("input", "r");

    while (fgets(buf, sizeof(buf), f))
    {
        for (int i = 0; i < 8; i++)
        {
            if (c >= i && c < i+3)
            {
                windows[i] += atoi(buf);
            }
        }

        c++;

        // vergleich
        if (c == 10)
        {
            for (int i = 1; i < 8; i++)
            {
                printf("window %d: %d\n", i, windows[i]);
                if (windows[i] > windows[i-1])
                {
                    larger++;
                }
            }

            for (int i = 0; i < 8; i++)
                windows[i] = 0;

            c = 0;
        }
    }

    printf("larger: %d\n", larger);

    return 0;
}
