#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>

int64_t pow_int64(int64_t a, int64_t b)
{
    int64_t r = 1;

    for (int64_t i = 0; i < b; i++)
    {
        r = r*a;
    }

    return r;
}

int count_line_len(char* line)
{
    int i = 0;
    char c = 1;
    while (c != '\n' && c != '\0')
    {
        c = line[i];
        i++;
    }

    return i-1;
}

int main(int argc, char** argv)
{
    if (argc < 2) {
        printf("bro du musst schon das file als arg geben\n");
        exit(1);
    }

    int line_len = 0;

    FILE* f = fopen(argv[1], "r");
    int64_t* bits_0;
    int64_t* bits_1;
    int line = 0;
    char buf[64];

    int64_t r1 = 0; // gamma
    int64_t r2 = 0; // epsilon

    while (fgets(buf, sizeof(buf), f))
    {
        if (line_len == 0)
        {
            line_len = count_line_len(buf);
            bits_0 = (int64_t*) calloc(line_len, sizeof(int64_t));
            bits_1 = (int64_t*) calloc(line_len, sizeof(int64_t));
        }

        for (int i = 0; i < line_len; i++)
        {
            if (buf[i] == '0')
            {
                bits_0[i]++;
            }
            else if (buf[i] == '1')
            {
                bits_1[i]++;
            }
        }
    }

    printf("ll: %d\nbits_0: ", line_len);
    for (int i = 0; i < line_len; i++)
        printf("%d ", bits_0[i]);
    printf("\nbits_1: ");
    for (int i = 0; i < line_len; i++)
        printf("%d ", bits_1[i]);
    printf("\n");

    for (int i = 0; i < line_len; i++)
    {
        if (bits_1[i] > bits_0[i])
            r1 += pow_int64(2, line_len-i-1);
        else
            r2 += pow_int64(2, line_len-i-1);
    }

    printf("gamma: %ld, epsilon: %ld, g*e: %ld\n", r1, r2, r1*r2);

    fclose(f);
    return 0;
}
