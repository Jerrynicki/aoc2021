#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>

#define LEN 12

int64_t pow_int64(int64_t a, int64_t b)
{
    int64_t r = 1;

    for (int64_t i = 0; i < b; i++)
    {
        r = r*a;
    }

    return r;
}

void find_most_common(char** nums, int numsn, bool* result)
{
    int64_t r0[LEN] = {0};
    int64_t r1[LEN] = {0};

    for (int i = 0; i < numsn; i++)
    {
        for (int j = 0; j < LEN; j++)
        {
            if (nums[i][j] == '0')
                r0[j]++;
            else if (nums[i][j] == '1')
                r1[j]++;
        }
    }

    for (int i = 0; i < LEN; i++)
    {
        if (r1[i] > r0[i])
            result[i] = true;
        else
            result[i] = false;
    }
}

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        printf("bro du musst schon das file als arg geben\n");
        exit(1);
    }

    char buf[64];
    char* nums[16384];
    bool result[LEN];
    int64_t numsc = 0;

    FILE* f = fopen(argv[1], "r");

    // read nums
    while (fgets(buf, sizeof(buf), f))
    {
        if (!(strlen(buf) < LEN))
        {
            nums[numsc] = (char*) malloc(strlen(buf) + 1);
            strcpy(nums[numsc], buf);
            numsc++;
        }
    }

    find_most_common(nums, numsc, result);
    for (int i = 0; i < LEN; i++)
        printf("%d ", result[i]);
    putchar('\n');

    fclose(f);
    return 0;
}
