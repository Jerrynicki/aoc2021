#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

const char* FORWARD = "forward";
const char* DOWN = "down";
const char* UP = "up";

void split(char* input, char at, char* out1, char* out2)
{
    bool reached = false;
    int c = 0;

    for (int i = 0; i < strlen(input); i++)
    {
        if (input[i] == at && !reached)
        {
            reached = true;
            out1[c] = '\0';
            c = 0;
        }
        else
        {
            if (reached)
                out2[c] = input[i];
            else
                out1[c] = input[i];
            c++;
        }
    }

    out2[c] = '\0';
}

int main(int argc, char** argv)
{
    if (argc < 2) {
        printf("bro du musst schon das file als arg geben\n");
        exit(1);
    }

    FILE* f = fopen(argv[1], "r");
    int x,y = 0;
    char buf[64];
    char command[64];
    char num_buf[64];
    int num;

    while (fgets(buf, sizeof(buf), f))
    {
        split(buf, ' ', command, num_buf);
        num = atoi(num_buf);

        if (strcmp(command, FORWARD) == 0)
            x += num;
        else if (strcmp(command, DOWN) == 0)
            y += num;
        else if (strcmp(command, UP) == 0)
            y -= num;
        else
            printf("Error! Command %s not found\n", command);
    }

    printf("x: %d, y: %d, x*y: %d\n", x, y, x*y);

    fclose(f);
    return 0;
}
